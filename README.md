# plasmOES

This repo will host the code used in the experiments performed for the paper "A structured evaluation of regression models for predicting CO<sub>2</sub> concentration from plasma emission spectra" (currently under review). The recorded spectra will be uploaded to [Zenodo](https://zenodo.org), with link published here.

## Code Status
The code will be published after it has been sanitized (cleared of any absolute paths, personal details, etc) and paper has been published.

## Data Status
Relevant metadata and usage instructions are being compiled.

